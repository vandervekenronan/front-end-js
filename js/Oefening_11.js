function leesGetal() {
	var g;
	do {
		g = +prompt("Geef getal in tss 1 en 20");
	} while (!(0<g && g<21))
	
	document.getElementById('head').innerHTML = "Tafel van "+g;
	return g;
}

function berekenTafels(g) {
	var t = [];
	for (var i = 1; i <= 20; i++) {
		t.push(i+" * "+g+" = "+i*g + "<br />");
	}
	t = t.join("");
	return t;
}

function toonTafels(g) {
	document.getElementById('tafel').innerHTML = berekenTafels(g);
}

toonTafels(leesGetal());