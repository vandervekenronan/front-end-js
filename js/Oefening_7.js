var g1 = (prompt('Getal 1: '));
var g2 = (prompt('Getal 2: '));

if (isNaN(g1) || isNaN(g2) || g1=="" || g2=="" || g1==null || g2==null) {
	document.getElementById("res").innerHTML = "Je hebt geen 2 getallen ingegeven";
}
/*
OF
do {
	var g1 = (prompt('Getal 1: '));
	var g2 = (prompt('Getal 2: '));
} while(isNaN(g1) || isNaN(g2) || g1=="" || g2=="" || g1==null || g2==null);
*/

else {
	g1 = +g1;
	g2 = +g2;

	if (g1==g2) {
		document.getElementById("res").innerHTML = "Getal 1: ("+g1+") is gelijk aan het tweede getal: ("+g2+").";
	}
	else if (g1>g2) {
		document.getElementById("res").innerHTML = "Getal 1: ("+g1+") is groter dan het tweede getal: ("+g2+").";
	}
	else {
		document.getElementById("res").innerHTML = "Getal 1: ("+g1+") is kleiner dan het tweede getal: ("+g2+").";
	}
}