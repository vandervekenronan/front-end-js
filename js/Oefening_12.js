/*
    VAN DER VEKEN RONAN
    14-2-19 FRONT-END JS
    OEFENING 9 10 11 !
*/

/*
func maaktafel
Maakt de vermenigvuldigingstabel voor een gegeven waarde g.
*/
function maakTafel(g) {
    var tabel = new SomArrayay();
    var somArray = new SomArrayay();
    tabel.push("<hr />");

    for(var i=1; i<=20; i++) {
        tabel.push(i+" * "+g+" = "+i*g);
        somArray.push(i*g);
    }
    // Voor de toonSom1 & toonSom2 functies
    tabel.push("<hr />");
    tabel.push("toonSom1: " + toonSom1(somArray));
    tabel.push("toonSom2: " + toonSom2(somArray));
    tabel.push("<hr />");
    return tabel.join("<br />");
}

/*
func leesGetal
Leest getal in dat moet liggen tss de 1 en 20; anders prompt hij verder!
*/
function leesGetal() {
    while(!(1 <= g && g <= 20)) {
        var g = +(prompt("Geef getal in tss 1 en 20 "));
    } 
    return g;
}

/*
func toonSom1
for lus om elementen op te tellen
    */
let toonSom1 = (somArray) => {
    var som = 0;
    for(var i = 0; i < (somArray.length); i++) {
        som += somArray[i];
    }
    return som;
}

/*
func toonSom2
SomArrayay.reduce om somArrayay elementen te sommeren
*/
let toonSom2 = (somArray) => {
    //const reducer = (collector, currentVal) => collector + currentVal;
    //return somArray.reduce(reducer);

    var som = somArray.reduce(function(a, b){return a+b;}, 0);
    return som;
}

function main() {
    let g = leesGetal();
    let t = maakTafel(g);

    document.getElementById("Head1").innerHTML = "Tafel van "+g;
    document.getElementById("tafel").innerHTML = t;    
}
main()
